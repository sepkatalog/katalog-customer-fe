import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemService } from 'src/app/core/services/item.service';

@Component({
  selector: 'app-order-home',
  templateUrl: './order-home.component.html',
  styleUrls: ['./order-home.component.scss']
})
export class OrderHomeComponent implements OnInit {

  form: FormGroup = this.fb.group({
    qty: [],
    firstname: [],
    lastname: [],
    email: [],
    contact: [],
    address: [],
    productid: [],
    createdBy: []
  });

  item: any;

  constructor(
    private itemService: ItemService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.getCurrentRoute();
  }

  getCurrentRoute() {
    let itemId = this.activatedRoute.snapshot.url[0].path;
    this.getItem(itemId);
  }

  getItem(id: any) {
    this.itemService.getItemById(id)
      .subscribe(
        (res) => {
          this.item = res;
        }
      );
  }
  placeOrder(errorModalData) {
    this.form.patchValue({
      productid: this.item.id,
      createdBy: this.form.value.email
    })
    this.itemService.placeOrder(this.form.value)
      .subscribe(
        (res) => {
          this.openSuccessModal(errorModalData);
        }
      );
  }
  openSuccessModal(errorModalData) {
    this.modalService.open(errorModalData, { centered: true });
  }
  closeModal() {
    this.modalService.dismissAll();
    this.router.navigate(["/"]);
  }
}
