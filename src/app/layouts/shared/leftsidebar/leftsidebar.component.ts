import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

import { AuthenticationService } from '../../../core/services/auth.service';
import { SIDEBAR_WIDTH_CONDENSED } from '../../layout.model';

@Component({
  selector: 'app-leftsidebar',
  templateUrl: './leftsidebar.component.html',
  styleUrls: ['./leftsidebar.component.scss'],

})
export class LeftsidebarComponent implements OnInit {

  @Input() sidebarType: string;
  @Input() menuItems: any;
  @Output() selectedItem = new EventEmitter();

  constructor(
    private keyCloakService: KeycloakService
  ) { }

  ngOnInit() {

  }

  /**
   * Is sidebar condensed?
   */
  isSidebarCondensed() {
    return this.sidebarType === SIDEBAR_WIDTH_CONDENSED;
  }

  /**
   * Logout the user
   */
  logout() {
    this.keyCloakService.logout();
    this.keyCloakService.login();
  }
  passSelectedItem(event) {
    this.selectedItem.emit(event);
  }
}
