import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    {
        label: 'Navigation',
        isTitle: true
    },
    {
        label: 'Home',
        icon: 'home',
        link: '/home'
    },
    // {
    //     label: 'About Us',
    //     icon: 'home',
    //     link: '/about'
    // },
    {
        label: 'Contact Us',
        icon: 'home',
        link: '/contact-us'
    }
];
