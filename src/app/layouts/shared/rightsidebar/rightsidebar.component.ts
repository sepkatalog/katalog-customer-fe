import { Component, ComponentFactoryResolver, OnInit, ViewChild } from '@angular/core';
import { Action } from 'src/app/core/models/action';
import { FormService } from 'src/app/core/services/form.service';

import { EventService } from '../../../core/services/event.service';
import { AnchorFormDirective } from './anchor-form.directive';

@Component({
  selector: 'app-rightsidebar',
  templateUrl: './rightsidebar.component.html',
  styleUrls: ['./rightsidebar.component.scss']
})
export class RightsidebarComponent implements OnInit {

  @ViewChild(AnchorFormDirective, { static: true }) formHost!: AnchorFormDirective;
  formName: string;

  constructor(
    private eventService: EventService,
    private resolver: ComponentFactoryResolver,
    private formService: FormService
  ) { }

  ngOnInit() {
    this.getComponent();
  }

  getComponent() {
    this.formService.initialFormContainer
      .subscribe(
        (data: any) => {
          if (!!data) {
            this.formName = data.formName;
            this.AddForm(data.component, data.data, data.type);
          }
        }
      );
  }

  AddForm(component: any, data: any, type: Action) {
    const viewContainerRef = this.formHost.viewContainerRef;
    viewContainerRef.clear();
    const factory = this.resolver.resolveComponentFactory(component)
    const componentRef: any = viewContainerRef.createComponent(factory);
    componentRef.instance.type = type;
    switch (type) {
      case Action.DELETE:
      case Action.UDPATE:
      case Action.VIEW:
        componentRef.instance.data = data;
        break;
      default:
        componentRef.instance.data = null;
        break;
    }
  }

  /**
   * Change the layout onclick
   * @param layout Change the layout
   */
  changeLayout(layout: string) {
    this.eventService.broadcast('changeLeftSidebarType', 'default');
    this.eventService.broadcast('changeLeftSidebarTheme', 'default');
    this.eventService.broadcast('changeLayoutWidth', 'default');
    this.eventService.broadcast('changeLayout', layout);
    this.hide();
  }

  /**
   * Change the left-sidebar theme
   * @param theme Change the theme
   */
  changeLeftSidebarTheme(theme: string) {
    this.eventService.broadcast('changeLayout', 'vertical');
    this.eventService.broadcast('changeLeftSidebarTheme', theme);
    this.eventService.broadcast('changeLeftSidebarType', 'default');
    this.hide();
  }

  /**
   * Change the layout
   * @param type Change the layout type
   */
  changeLeftSidebarType(type: string) {
    this.eventService.broadcast('changeLayout', 'vertical');
    this.eventService.broadcast('changeLeftSidebarType', type);
    this.hide();
  }

  /**
   * Change the layout width
   * @param width string 
   */
  changeLayoutWidth(width: string) {
    this.eventService.broadcast('changeLayoutWidth', width);
    this.hide();
  }


  /**
   * Hide the sidebar
   */
  public hide() {
    document.body.classList.remove('right-bar-enabled');
  }
}
