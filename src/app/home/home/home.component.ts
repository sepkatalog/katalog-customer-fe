import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from 'src/app/core/models/item.models';
import { CategoryService } from 'src/app/core/services/category.service';
import { ItemService } from 'src/app/core/services/item.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  categories: any[] = [{ label: "All", id: null, code: null }];

  items: Item[] = [];
  currentPage: number = 0;
  currentCategory: number;
  isListView :boolean = false;

  constructor(
    private itemService: ItemService,
    private categoryService: CategoryService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getCategories();
    this.getItems(`?page=${this.currentPage}&limit=10`);
    this.subscribeToSearch();
  }
  subscribeToSearch() {
    this.itemService.initialsearchItemSource
      .subscribe(
        (term) => {
          if (this.currentCategory && term) {
            this.getItems(`?kws=${term}&cat=${this.currentCategory}`);
          } else {
            this.getItems(`?page=${this.currentPage}&limit=10`);
          }
        });
  }
  getCategories() {
    this.categoryService.getCategory()
      .subscribe(
        (res: any[]) => {
          let list = res.map(
            (cat) => {
              return {
                label: cat.title,
                id: cat.id,
                code: cat.code
              }
            }
          )
          this.categories = [...this.categories, ...list]
        }
      );
  }

  getItems(query: string) {
    this.itemService.getItems(query)
      .subscribe(
        (res: any[]) => {
          this.items = res.map(
            (data) => {
              return {
                ...data,
                image: data.image || "assets/images/default.png"
              }
            }
          );
        }
      );
  }

  passSelectedItem(item) {
    if (item.id) {
      this.currentCategory = item.id;
      this.getItems(`?page=${this.currentPage}&limit=10&cat=${item.id}`);
    } else {
      this.getItems(`?page=${this.currentPage}&limit=10`);
    }
  }
  pageChange(isPrev: boolean, change) {
    if (isPrev || (!isPrev && this.items.length > 0)) {
      this.currentPage += change;
      let params = (this.currentCategory) ? `?page=${this.currentPage}&limit=10&cat=${this.currentCategory}` : `?page=${this.currentPage}&limit=10`;
      if (this.currentPage > -1) {
        this.getItems(params);
      } else {
        this.currentPage = 0;
      }
    }
  }
  navigateToDetailView(item) {
    this.router.navigate(["order", item.id]);
  }
}
