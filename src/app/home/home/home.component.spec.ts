import { HttpClient, HttpHandler } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { KeycloakService } from 'keycloak-angular';
import { BehaviorSubject, of } from 'rxjs';
import { CategoryService } from 'src/app/core/services/category.service';
import { ItemService } from 'src/app/core/services/item.service';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { ItemComponent } from '../item/item.component';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  const itemServiceMock: jasmine.SpyObj<ItemService> = jasmine.createSpyObj("ItemService", ["addItem", "getCategory","updateItem","deleteItem","getItems","initialsearchItemSource"]);
  const categoryServiceMock: jasmine.SpyObj<CategoryService> = jasmine.createSpyObj("CategoryService", ["getCategory"]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent , ItemComponent],
      imports:[
        LayoutsModule
      ],
      providers:[
        KeycloakService,
        RouterTestingModule,
        { provide: ItemService, useValue: itemServiceMock },
        { provide: CategoryService, useValue: categoryServiceMock },
        HttpClient,
        HttpHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    itemServiceMock.initialsearchItemSource = new BehaviorSubject(null).asObservable();
    itemServiceMock.getItems.and.returnValue(of([] as any));
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it("Should call getCategories",()=>{
    categoryServiceMock.getCategory.and.returnValue(of([]));
    component.getCategories();
    expect(categoryServiceMock.getCategory).toHaveBeenCalled();
  });
  it("Should call getItems",()=>{
    itemServiceMock.getItems.and.returnValue(of([]));
    component.getItems("");
    expect(itemServiceMock.getItems).toHaveBeenCalled();
  });
  it("Should call passSelectedItem",()=>{
    spyOn(component,"getItems");
    component.passSelectedItem({id:1});
    expect(component.getItems).toHaveBeenCalled();
  })
  it("Should call passSelectedItem",()=>{
    spyOn(component,"getItems");
    component.passSelectedItem({id:null});
    expect(component.getItems).toHaveBeenCalled();
  })
  it("Should call pageChange",()=>{
    spyOn(component,"getItems");
    component.pageChange(true,1);
    expect(component.getItems).toHaveBeenCalled();
  });
});
