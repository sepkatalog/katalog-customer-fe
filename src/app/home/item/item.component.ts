import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() data: any;
  @Output() onViewClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onViewEventClick() {
    this.onViewClick.emit();
  }

}
