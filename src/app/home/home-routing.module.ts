import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "contact-us", loadChildren: () => import('../contact-us/contact-us.module').then(m => m.ContactUsModule) },
  // { path: "about", loadChildren: () => import('../about-us/about-us.module').then(m => m.AboutUsModule) },
  { path: "", redirectTo: "home" },
  { path: "order", loadChildren: () => import("../order/order.module").then(m => m.OrderModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
