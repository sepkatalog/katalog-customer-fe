import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { LayoutsModule } from '../layouts/layouts.module';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';


@NgModule({
  declarations: [HomeComponent, CategoryListComponent, ItemComponent, ItemListComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    LayoutsModule
  ]
})
export class HomeModule { }
