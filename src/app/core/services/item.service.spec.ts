import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Item } from '../models/item.models';

import { ItemService } from './item.service';

describe('ItemService', () => {
  let service: ItemService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]
  }));

  beforeEach(()=>{
    service = TestBed.get(ItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it("Should call addItem", () => {
    service.addItem({} as Item);
  });
});
