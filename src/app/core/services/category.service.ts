import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  api = `/api/v1`;

  constructor(
    private httpClient: HttpClient
  ) { }

  getCategory(): Observable<any> {
    return this.httpClient.get(`${this.api}/category`);
  }
}
