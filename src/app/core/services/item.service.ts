import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Item } from '../models/item.models';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  api = `/api/v1`;

  public searchItemSource = new BehaviorSubject(null);
  initialsearchItemSource = this.searchItemSource.asObservable();

  constructor(
    private httpClient: HttpClient
  ) { }

  getItems(queryparams: string): Observable<any> {
    let url: string = `${this.api}/item${queryparams}`
    return this.httpClient.get(url);
  }
  addItem(payload: Item): Observable<Item> {
    return this.httpClient.post<Item>(`${this.api}/item`, payload);
  }
  passSearchKeyWord(data: any) {
    this.searchItemSource.observers = this.searchItemSource.observers.slice(this.searchItemSource.observers.length - 1);
    this.searchItemSource.next(data);
  }
  getItemById(id: any): Observable<any> {
    return this.httpClient.get(`${this.api}/item/${id}`)
  }
  placeOrder(payload): Observable<any> {
    return this.httpClient.post(`${this.api}/order`, payload);
  }
}
