import { TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';

import { FormService } from './form.service';

describe('FormService', () => {
  let service: FormService;
  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(()=>{
    service = TestBed.get(FormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("Should call setFormComponent", () => {
    service.formContainerSource = new BehaviorSubject(null)
    spyOn(service.formContainerSource, "next");
    service.setFormComponent({});
    expect(service.formContainerSource.next).toHaveBeenCalled();
  });

});
