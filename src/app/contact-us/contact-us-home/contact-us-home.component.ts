import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserProfileService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-contact-us-home',
  templateUrl: './contact-us-home.component.html',
  styleUrls: ['./contact-us-home.component.scss']
})
export class ContactUsHomeComponent implements OnInit {

  form: FormGroup = this.fb.group({
    itemCode: [],
    firstname: [],
    contact: [],
    address: [],
    lastname: [],
    email: [],
    createdBy: [],
    message: [],
  })

  constructor(
    private fb: FormBuilder,
    private userService: UserProfileService
  ) { }

  ngOnInit() {
  }

  sendRequest() {
    this.form.patchValue({createdBy : this.form.value.email});
    this.userService.sendContactUsEmail(this.form.value)
      .subscribe(
        (res) => {
          this.form.reset();
        }
      );
  }
}
